---
date: 2022-03-03
title: Season of KDE 2022 With KDE Eco
categories: [SOK22, Standard Usage Scenarios, Automation Tools, Blauer Engel]
author: Karanjot Singh
summary: My experience testing automation tools and preparing usage scenarios as a Season of KDE student with KDE Eco.
scssFiles:
  - scss/20220303.scss
SPDX-License-Identifier: CC-BY-SA-4.0
authors:
- SPDX-FileCopyrightText: 2022 Karanjot Singh <drquark@duck.com>
---

I am very thankful to the KDE team for inviting me to be a part of this amazing community through their annual program [Season of KDE](https://season.kde.org).

#### About Me

I am Karanjot Singh. I am a first-year computer science engineering undergraduate from Jaypee Institute of Information Technology, Noida, India. I have worked as a developer with various Free and Open-Source Software (FOSS) programs such as Kharagpur Winter of Code in 2021 (KWoC’21). However, this will be my first time contributing to a project with such a large community. I am very passionate about FOSS, solving problems, and improving process efficiency. I enjoy exploring and learning new technologies and building things for socially-useful causes. I also believe that contributing to FOSS is the best way to gain experience in software development.

#### What I Will Be Working On

As part of a pioneering sustainability project, [KDE Eco](https://eco.kde.org/) has the aim of measuring and driving down the energy consumption of KDE/Free Software. This requires emulating user behaviour, which can be achieved by planning and scripting Standard Usage Scenarios. I will be scripting Standard Usage Scenarios for various applications, with a focus on commonly-used text editors like [Kate](https://apps.kde.org/kate/), [KWrite](https://apps.kde.org/kwrite/), [Vim](https://www.vim.org/), [Nano](https://www.nano-editor.org/), [Emacs](https://www.gnu.org/software/emacs/), [Calligra Words](https://apps.kde.org/calligrawords/), and [LibreOffice](https://www.libreoffice.org/). I will prepare these usage scenarios with one of many available [emulation tools](https://invent.kde.org/teams/eco/feep/-/blob/master/tools/presentation_Visual_Workflow_Automation_Tools/Visual_Workflow_Automation_Tools.pdf). Below is my SoK’22 Timeline:

{{< container class="text-center" >}}

![Season of KDE 2022 Timeline](/blog/images/SoK_22Timeline.png)

{{< /container >}}


#### Motivation Behind Working On The KDE Eco Project

I think KDE Eco is a very good initiative for developing resource and energy-efficient Free Software. When someone uses FOSS and discovers that the software is transparent about its energy consumption, that using the software may help prolong the lifespan of hardware and reduce the environmental impact of digitization, this excites me. I always want to be part of something that contributes to future generations and KDE Eco serves that purpose. I also believe that KDE Eco assists me in my journey to become a better software developer.

#### What I Have Learnt So Far

{{< img src="/blog/images/whathow.png" img_class="width-300px" >}}

When I was beginning with this project, I had three questions.

*How can we know when software is resource and energy-efficient?*

*How can we measure software’s energy consumption?*

*And does it make a difference?*

These questions may come into anyone’s mind when thinking about the idea of software efficiency.

When you use software, what you see is only the interface you are interacting with. Just tell your phone what you want and things will appear magically — whether it is the information on screen or a package delivered to your doorstep. But the technologies and infrastructure behind it are determined by software engineers who make all of this work.

{{< container class="text-center" >}}

![UI vs. Complex Processes](/blog/images/UIvsProcesses.png)

{{< /container >}}

Just imagine: What if we analyzed the energy consumption behind commonly-used software and made it more transparent? What if users could learn how much energy their software requires and could choose the application that might be better for the environment? This would be great!!!

The [KDE Eco](https://invent.kde.org/teams/eco) initiatives Free and open source Energy Efficiency Project ([FEEP](https://invent.kde.org/teams/eco/feep)) and Blauer Engel For FOSS ([BE4FOSS](https://invent.kde.org/teams/eco/be4foss)) are working hard on these issues.

As noted by [FEEP](https://eco.kde.org/#feep), the design and implementation of software has a significant impact on the energy consumption of the systems it is part of. With the right tools, it is possible to quantify and drive down energy consumption. This increased efficiency contributes to a more sustainable use of energy as one of the shared resources of our planet.

{{< container class="text-center" >}}

![3 Steps of Eco Certification](/blog/images/3StepsToBEECO.png)

{{< /container >}}

BE4FOSS supports FEEP by collecting and spreading information related to Blauer Engel (BE) eco-certification, the official environmental label awarded by the German government. As stated at the [KDE Eco website](https://eco.kde.org/#be4foss), obtaining the Blauer Engel label occurs in 3 steps: (1) Measure, (2) Analyze, (3) Certify.

1. **MEASURE** in dedicated labs, such as at KDAB Berlin
2. **ANALYZE** using statistical tools such as OSCAR (Open-source Software Consumption Analysis in R)
3. **CERTIFY** by submitting the report on the fulfilment of the Blauer Engel criteria

In SoK'22, I will be preparing Standard Usage Scenarios for various text editors so that the usage scenarios may be used in Step 1 for obtaining BE eco-certification.

#### What I Have Done & Will Be Doing In The Coming Weeks

For the past three weeks, I have tested various automation tools, in particular ``Actiona``, ``xdotool``, and ``GNU Xnee``, to decide which of these tools would be best for implementing Standard Usage Scenarios.

While using [``Actiona``](https://github.com/Jmgr/actiona), I wrote some documentation so that this information will also be beneficial for anyone wanting to contribute to KDE Eco.

While trying out [``xdotool``](https://github.com/jordansissel/xdotool), I came across an issue with memory leak. Running Valgrind on xdotool search, I get more lost memory allocated by XQueryTree. There are probably other places where memory is allocated and not being freed afterwards. I have not done an extensive check. Nevertheless, xdotool gives more control over the system and even with some issues, this is the tool that I found most helpful.

I tested [``GNU Xnee``](https://xnee.wordpress.com/) too, which seemed interesting to me as it records the output and stores it in a separate file. Also, it provides a replayer that one can use to automate tasks at whatever speed one wants.

In the end, I have decided to prepare all usage scenarios with xdotool, at least initially, as most of the text editors use keyboard functions rather than mouse activity and this will make it easier to adapt the script to different systems. However, after SoK'22 I plan to prepare usage scenarios with Actiona as well so that I can learn the differences between these tools. On a related note, there is also an ongoing discussion about [repurposing Standard Usage Scenarios](https://invent.kde.org/teams/eco/be4foss/-/issues/31) for testing responsiveness and performance on older or lower-end hardware.

{{< container class="text-center" >}}

![Preparing Standard Usage Scenarios](/blog/images/PreparingSUS.png)

{{< /container >}}

In the coming weeks, I will additionally be working on fixing an issue that is known for Actiona. The problem is that Actiona emulates user behaviour by storing the position of clicks based on pixel coordinates, which can make transferring a script to another system challenging. For instance, this issue arose recently when preparing a Standard Usage Scenario for [GCompris](https://apps.kde.org/gcompris/). I came up with a solution of resizing the screen to a minimum screen resolution and including the code at the beginning of the Actiona script. Whenever the script runs in a different system, it automatically resizes to the minimum resolution to match the pixels while making the script. This is just an idea and is not yet tested. I will put aside time for this attempt to fix the problem with the help of the GCompris team.

The GitLab repository containing Standard Usage Scenarios in progress (currently GCompris using Actiona) can be found [here](https://invent.kde.org/teams/eco/be4foss/-/tree/master/standard-usage-scenarios).

#### Community Bonding ( SoK’22 )

I am thankful to my mentor Joseph P. De Veaugh-Geiss for taking time to help me by providing resources and guidance during the project. I also attended the KDE Eco monthly community meetup on [9 February 2022](https://invent.kde.org/teams/eco/be4foss/-/blob/master/community-meetups/2022-02-09_community-meetup_protocol.md), where Prof. Dr Stefan Naumann, Achim Guldner, and Christopher Stumpf from [Umwelt Campus Birkenfeld](https://www.umwelt-campus.de/forschung/projekte/green-software-engineering/home) lead a [discussion](https://invent.kde.org/teams/eco/be4foss/-/blob/master/community-meetups/2022-02-09_community-meetup_umwelt-campus-presentation.pdf) on measuring the energy consumption of distributed systems and extending the Blauer Engel award criteria to them. This was an interesting meeting where we discussed problems for automation with mobile applications as well as client-server testing. Joseph also introduced me to the Umwelt Campus researchers who measured the [energy consumption](https://invent.kde.org/teams/eco/feep/-/tree/master/measurements/okular) of [Okular](https://okular.kde.org); Emmanuel Charruau, who is working with the GCompris script; and Nicolas Fella, who is currently working with xdotool for his Master's thesis on the energy consumption of software.

{{< container class="text-center" >}}

{{< img src="/blog/images/communitybonding.png" img_class="width-300px" >}}

{{< /container >}}

Thank you for taking time to read this update. If anyone wants to follow-up on the ideas presented here regarding the pros and cons of different automation tools when implementing Standard Usage Scenarios, there is a [GitLab issue](https://invent.kde.org/teams/eco/be4foss/-/issues/32) at the BE4FOSS repository where we can discuss further.

I am also available on KDE's Matrix instance at drquark:kde.org.
